package Main;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;

import org.opencv.core.Mat;
import org.opencv.core.Size;
import org.opencv.imgcodecs.Imgcodecs;
//import org.opencv.highgui.Highgui;
//import org.opencv.highgui.VideoCapture;
import org.opencv.imgproc.Imgproc;
import org.opencv.videoio.VideoCapture;

public class Create_Negatives {

	private VideoCapture camera = null;

	public Create_Negatives(int source){
		this.camera = new VideoCapture(source);
	}

	public Create_Negatives(String source){
		this.camera = new VideoCapture(source);
	}

	public boolean create(int number){

		Mat frame = new Mat();
		Mat resized = new Mat();

		//getting the number of files inside folder not to overwrite existing negatives
		int before = new File("./negatives").list().length;
        int i = before + 1;

		if (!camera.isOpened()){
			System.out.println("Error 1");
			camera.release();
			return false;
		} else {

			//always add 2000 or more? images
			while(i < before + number){

				if(camera.grab()){

					i++; //just for the name

					camera.retrieve(frame);

					Imgproc.cvtColor(frame, frame, Imgproc.COLOR_BGR2GRAY);

					Imgproc.resize( frame, resized, new Size(200, 200));

					Imgcodecs.imwrite("./negatives/"+i+".bmp", resized);

					try {
						Thread.sleep(10);
					} catch (InterruptedException e) {
						e.printStackTrace();
						return false;
					}

				}
			}
			camera.release();
		}

		//creating the negative.txt file with references to our negative pics
		try {
			generate_list_file("./negatives");
		} catch (IOException e) {
			e.printStackTrace();
			return false;
		}
		return true;
	}

	//http://stackoverflow.com/questions/14676407/list-all-files-in-the-folder-and-also-sub-folders#14676464
    private void generate_list_file(String directoryName) throws IOException {
        File directory = new File(directoryName);

        BufferedWriter writer = new BufferedWriter( new FileWriter("./negatives.txt") );

        StringBuilder builder = new StringBuilder("");

        // get all the files from a directory
        File[] fList = directory.listFiles();
        for (File file : fList) {
            if (file.isFile()) {
                //System.out.println(file.getAbsolutePath());
                builder.append(file.getAbsolutePath().toString() + "\n");

            }
        }
        System.out.println(builder.toString());
        writer.write(builder.toString());
        writer.write("\n");
        writer.close();

    }

}
