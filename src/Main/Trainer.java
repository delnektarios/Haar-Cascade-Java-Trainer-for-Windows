//DO NOT ALTER THE CODE

package Main;

import java.awt.EventQueue;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;

import javax.swing.JOptionPane;

import org.opencv.core.Core;



public class Trainer {

	private static int number_of_negs = 30;
	private static Positives_Creator_panel window = null;

	public static void main(String[] args) {

    	//load the OpenCV Native Library previously set on our project
		//opencv 3 in use here!!!!
        System.loadLibrary(Core.NATIVE_LIBRARY_NAME);

		boolean created_negatives = new File("./negatives").mkdirs();
		boolean created_info = new File("./info").mkdirs();

		File f = new File("./data");
		if(f.exists() && f.isDirectory()) {
			JOptionPane.showMessageDialog(null,
				    "In case you want to train a different object detector"
				    + " delete folder ./\"data\""
				    + "in project folder!",
				    "Possible train Failure!",
				    JOptionPane.WARNING_MESSAGE);
		}

		boolean created_data = new File("./data").mkdirs();

		if ( created_negatives || created_info ||  created_data ){
			System.out.println("Directories created successfully.");
		} else {
			System.out.println("Creation of directories failed!");
		}

		Create_Negatives negS = new Create_Negatives(0);
		//argument from dsl number
		negS.create(number_of_negs);

		//to halt main thread until we collect the positive samples
		Object lock = new Object();

    	int option = JOptionPane.showConfirmDialog(null,
    			"Do you want to create positive samples?",
    			"Positive Samples Creator",
    			JOptionPane.YES_NO_OPTION,
    			JOptionPane.QUESTION_MESSAGE);


    	if (option == JOptionPane.YES_OPTION){
    		System.out.println("yes");

    		EventQueue.invokeLater(new Runnable() {
    			public void run() {
    				try {
    					window = new Positives_Creator_panel(lock);
    					window.getFrame().setVisible(true);
    				} catch (Exception e) {
    					e.printStackTrace();
    				}
    			}
    		});

        	synchronized (lock) {
        	    try {
    				lock.wait();
    			} catch (InterruptedException e) {
    				e.printStackTrace();
    			}
        	}

    	}else{
    		System.out.println("no");
    	}


    	System.out.println("continues......");

    	//using 85% of the available negatives to avoid misses in opencv_createsamples.exe algorithm
    	int avail_negs = new File("./negatives").list().length;
    	int negatives_count = (int) (avail_negs - (avail_negs*0.15));



    	try {
			create_samples_bat_file("C:/Users/netarios/workspace/My7_Haar_Cascade_Trainer", 20, 20, 0.7, 0.7, 0.7, negatives_count);

			create_test_samples_bat_file("C:/Users/netarios/workspace/My7_Haar_Cascade_Trainer", 20, 20, 0.7, 0.7, 0.7, negatives_count);

			train_cascade_bat_file("C:/Users/netarios/workspace/My7_Haar_Cascade_Trainer", 20, 20, 100, negatives_count, 20, 0.999, 0.5);

			see_vector_file_bat("C:/Users/netarios/workspace/My7_Haar_Cascade_Trainer", 20, 20);

		} catch (IOException e) {
			e.printStackTrace();
		}


    	//haar_cascade_executor(".", "java_create_samples.bat");

    	//haar_cascade_executor(".", "java_create_test_samples.bat");

    	//haar_cascade_executor(".", "java_train_cascade.bat");

	}

	public static void create_samples_bat_file(String opencv_path, int width, int hight,
			double maxxangle, double maxyangle, double maxzangle, int num) throws IOException{

		BufferedWriter writer = new BufferedWriter( new FileWriter("./java_create_samples.bat"));
		writer.write(
				(
				opencv_path
				+ "/opencv/build/x64/vc12/bin/opencv_createsamples.exe"
				+ " -info ./HaarTools/info.txt"
				+ " -vec positives.vec"
				+ " -bg negatives.txt"
				+ " -w " + width
				+ " -h " + hight
				+ " -maxxangle " + maxxangle
				+ " -maxyangle " + maxyangle
				+ " -maxzangle " + maxzangle
				+ " -num " + num
				).replace("/", "\\")
				);

		writer.close();
	}

	public static void create_test_samples_bat_file(String opencv_path, int width, int hight,
			double maxxangle, double maxyangle, double maxzangle, int num) throws IOException{

		BufferedWriter writer = new BufferedWriter( new FileWriter("./java_create_test_samples.bat"));
		writer.write(
				(
				opencv_path
				+ "/opencv/build/x64/vc12/bin/opencv_createsamples.exe"
				+ " -bg negatives.txt"
				+ " -info ./HaarTools/info.txt"
				+ " -pngoutput info"
				+ " -w " + width
				+ " -h " + hight
				+ " -maxxangle " + maxxangle
				+ " -maxyangle " + maxyangle
				+ " -maxzangle " + maxzangle
				+ " -num " + num
				).replace("/", "\\")
				);

		writer.close();
	}

	public static void train_cascade_bat_file(String opencv_path, int width, int hight,
			int numPos, int numNeg, int numStages, double minHitRate, double maxFalseAlarmRate) throws IOException{
		//C:\Users\netarios\workspace\My7_Haar_Cascade_Trainer\opencv\build\x64\vc12\bin\opencv_traincascade.exe
		//-data data -vec positives.vec -bg negatives.txt -numPos 3000 -numNeg 1500 -numStages 15 -w 20 -h 20
		BufferedWriter writer = new BufferedWriter( new FileWriter("./java_train_cascade.bat"));
		writer.write(
				(
				opencv_path
				+ "/opencv/build/x64/vc12/bin/opencv_traincascade.exe"
				+ " -data data"
				+ " -vec positives.vec"
				+ " -bg negatives.txt"
				+ " -minHitRate " + minHitRate
				+ " -maxFalseAlarmRate " + maxFalseAlarmRate
				+ " -w " + width
				+ " -h " + hight
				+ " -numPos " + numPos
				+ " -numNeg " + numNeg
				+ " -numStages " + numStages
				).replace("/", "\\")
				);
		writer.close();
	}

	/**
	 * Method to create bat file to wathc the positives vector created by opencv create samples utility
	 * @param opencv_path path to executable file
	 * @param width
	 * @param hight
	 * @throws IOException
	 */
	public static void see_vector_file_bat(String opencv_path, int width, int hight) throws IOException{

		BufferedWriter writer = new BufferedWriter( new FileWriter("./java_see_vector.bat"));
		writer.write(
				(
				opencv_path
				+ "/opencv/build/x64/vc12/bin/opencv_createsamples.exe"
				+ " -vec positives.vec"
				+ " -bg negatives.txt"
				+ " -w " + width
				+ " -h " + hight
				).replace("/", "\\")
				);
		writer.close();
	}

	public static void cropper(){

		String path = "C:/Users/netarios/workspace/My7_Haar_Cascade_Trainer/HaarTools/objectmarker.exe";

		try {
			Process p = Runtime.getRuntime().exec("cmd /c start /wait " + path);
			p.waitFor();
		} catch (InterruptedException | IOException e) {
			e.printStackTrace();
		}
	}

	public static synchronized void haar_cascade_executor(String WORKING_DIR, String _bat_file){

		try {
			Process p = Runtime.getRuntime().exec("cmd /c start /wait " + _bat_file);
			p.waitFor();
		} catch (InterruptedException | IOException e) {
			e.printStackTrace();
		}
	}

}

//DO NOT ALTER THE CODE
