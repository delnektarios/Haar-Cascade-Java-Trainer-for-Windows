package Main;

import java.awt.Graphics;
import java.awt.Image;
import java.awt.image.BufferedImage;
import java.io.ByteArrayInputStream;
import java.io.IOException;

import javax.imageio.ImageIO;
import javax.swing.JPanel;

import org.opencv.core.Mat;
import org.opencv.core.MatOfByte;
import org.opencv.imgcodecs.Imgcodecs;
//import org.opencv.highgui.Highgui;
//import org.opencv.highgui.VideoCapture;
import org.opencv.videoio.VideoCapture;


public class Create_Positives implements Runnable {

	private volatile boolean runnable = false;

	private VideoCapture camera = new VideoCapture(0);
	private Mat frame = new Mat();
	private MatOfByte memory = new MatOfByte();
	private JPanel panel = null;

	private Mat image = new Mat();

	public Create_Positives(JPanel panel) {
		this.panel = panel;
	}

	@Override
	public void run() {
		this.start_capturing();
	}

	private void start_capturing(){

		if (!camera.isOpened()){
			System.out.println("Error 1");
			camera.release();
		} else {

			while(true){

				if(camera.grab()){

					camera.retrieve(frame);

					frame.copyTo(image);

					Imgcodecs.imencode(".bmp", frame, memory);
					Image image;

					if (this.runnable == false){
						return;
					}

					try {
						image = ImageIO.read(new ByteArrayInputStream(memory.toArray()));

						BufferedImage buffer = (BufferedImage) image;

						Graphics graphics = panel.getGraphics();

						if (graphics.drawImage(buffer, 0, 0, frame.width(), frame.height(), 0, 0, buffer.getWidth(), buffer.getHeight(), null)){}

					} catch (IOException e) {
						e.printStackTrace();
						runnable = false;
						camera.release();
						return;
					}

				}
			}
		}


	}

	public Mat getFrame() {
		return image;
	}

	public synchronized void setCaptureThread(boolean state){
		this.runnable = state;
	}

	public void releaseCamera(){
		camera.release();
	}

}