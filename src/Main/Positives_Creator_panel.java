package Main;

import javax.swing.JFrame;
import javax.swing.JOptionPane;
import javax.swing.JPanel;

import org.opencv.core.Mat;
//import org.opencv.highgui.Highgui;
import org.opencv.imgcodecs.Imgcodecs;

import javax.swing.JButton;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;

public class Positives_Creator_panel {

	private JFrame frame;
	private Create_Positives creator = null;
	private Object lock = null;

	/**
	 * Create the application.
	 * @param lock
	 */
	public Positives_Creator_panel(Object lock) {
		this.lock = lock;
		initialize();
	}

	/**
	 * Initialize the contents of the frame.
	 */
	private void initialize() {
		setFrame(new JFrame());
		getFrame().setBounds(100, 100, 625, 602);
		getFrame().setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		getFrame().getContentPane().setLayout(null);

		getFrame().addWindowListener(new java.awt.event.WindowAdapter() {
		    @Override
		    public void windowClosing(java.awt.event.WindowEvent windowEvent) {

		    	int option = JOptionPane.showConfirmDialog(getFrame(), "Are you sure to exit?","Exit", JOptionPane.YES_NO_OPTION,JOptionPane.QUESTION_MESSAGE);

		    	if (option == JOptionPane.YES_OPTION){

		    		System.out.println("exited from positives panel!!");

		    		creator.setCaptureThread(false);
		    		creator.releaseCamera();
		    		getFrame().dispose();

		        	synchronized (lock) {
		        		lock.notify();
		        	}

		    	}else{
		    		System.out.println("not exited");
		    	}

		    }
		});

		JPanel panel = new JPanel();
		panel.setBounds(12, 0, 583, 463);
		getFrame().getContentPane().add(panel);

		JButton capture_Button = new JButton("Snapshot");
		capture_Button.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {

				Mat frame = creator.getFrame();
				//Imgproc.resize( frame, frame, new Size(100, 100), 0.0, 0.0, Imgproc.INTER_LINEAR);
				Imgcodecs.imwrite("./HaarTools/rawdata/"+System.nanoTime()+".bmp", frame);
			}
		});
		capture_Button.setBounds(217, 476, 166, 41);
		getFrame().getContentPane().add(capture_Button);

		creator = new Create_Positives(panel);
		Thread thread = new Thread(creator);
		creator.setCaptureThread(true);
		thread.start();
	}

	public JFrame getFrame() {
		return frame;
	}

	public void setFrame(JFrame frame) {
		this.frame = frame;
	}
}
